/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)

// Use pi
 const smallPizzaPrice = 16.99;
 const largePizzaPrice = 19.99;
 const smallPizzaSize = 13.0;
 const largePizzaSize = 17.0;
 const smallPizzaArea = Math.PI * (smallPizzaSize / 2)**2;
 const largePizzaArea = Math.PI * (largePizzaSize / 2)**2;
 

// 2. What is the cost per square inch of each pizza?
const costPerSquareInchSmall = smallPizzaPrice / smallPizzaArea;
const costPerSquareInchLarge = largePizzaPrice / largePizzaArea;


// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)

//https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
getRandomInt(1, 13);

// 4. Draw 3 cards and use Math to determine the highest
// card
//Use math.max
// getRandomInt(1, 13);
const c1 = getRandomInt(1,13);
const c2 = getRandomInt(1,13);
const c3 = getRandomInt(1,13);
Math.max(c1, c2, c3);

/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

const firstName = "Lisa Renee"
const lastName = "Rowlett Leslie"
const streetAddress = "15715 NE 145th St"
const city = "Redmond"
const state = "Washington"
const zipCode = "98052"



// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring
 var findName = str.indexOf(" ")
 var subString = str.substring(0, findName);

/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00

const date1 = new Date('1/1/2020');
const date2 = new Date('4/1/2020');
const diffDate = (date2 - date1) / 2;

let newCalendarDay = new Date((date1.getTime() + date2.getTime()) / 2)
//
// Look online for documentation on Date objects.

// Starting hint:
const endDate = new Date(2019, 3, 1);
